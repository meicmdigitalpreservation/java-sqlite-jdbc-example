# README #

Example project showing how to use a SQLite database in a Java application. This project implements the tutorial from 
[Tutorials Point](http://www.tutorialspoint.com/sqlite/sqlite_java.htm) included in the ```doc``` folder.

### Set up and Run this project ###

##### Dependencies #####

This project is a [maven project](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html). 
Any IDE supporting maven projects should automatically recognise and download the dependencies needed to run the 
application.


##### Running the project in IntelliJ #####

1. Open the project in [IntelliJ](https://www.jetbrains.com/idea/).
2. Navigate to the class ```/src/main/java/com.mycompany.app/SQLiteJDBC.java``` in the project file browser.
3. Right click the file ```SQLiteJDBC.java``` and choose the option ```Run 'SQLiteJDBC.main()'```.
4. The application output will be shown in the ```Run``` panel located in the bottom of the IDE window.


## Author

**André Rosa**

* <https://bitbucket.org/candrelsrosa>
* <https://github.com/andreros/>
* <https://facebook.com/candrelsrosa>
* <https://twitter.com/candrelsrosa>


## License

Code released under [the GPL 3.0 license](https://opensource.org/licenses/GPL-3.0). Docs released under 
[Creative Commons](https://creativecommons.org/licenses/by/4.0/legalcode).